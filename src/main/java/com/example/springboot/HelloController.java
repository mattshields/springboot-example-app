package com.example.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.core.env.Environment;
import java.util.logging.Logger;

@RestController
public class HelloController {

	Logger logger = Logger.getLogger(HelloController.class.getName());

	@Autowired
    private Environment env;

	@GetMapping("/")
	public String index() {
		logger.info("Serving request from " + env.getProperty("db_username"));
		return "Greetings from Spring Boot!<br>\ndb_server: " + env.getProperty("db_server") + "<br>\ndb_username: " + env.getProperty("db_username") + "<br>\ndb_password: " + env.getProperty("db_password");
	}

}
