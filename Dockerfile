ARG MVN_VERSION=3.9.4
ARG JDK_VERSION=17
ARG APP_VERSION=1.0.2

FROM maven:${MVN_VERSION}-amazoncorretto-${JDK_VERSION} as build

ENV db_server=""
ENV db_username=""
ENV db_password=""

WORKDIR /build
COPY pom.xml .
# create a layer with all of the Maven dependencies, first time it takes a while consequent call are very fast
RUN yum -y install git
RUN mvn dependency:go-offline

COPY ./pom.xml /tmp/
COPY ./src /tmp/src/
WORKDIR /tmp/
# build the project
RUN mvn clean package

# extract JAR Layers
WORKDIR /tmp/target
RUN java -Djarmode=layertools -jar *.jar extract

# runtime image
FROM amazoncorretto:${JDK_VERSION} as runtime

WORKDIR /app

# copy layers from build image to runtime image as nonroot user
COPY --from=build /tmp/target/spring-boot-example-app-*.jar ./

EXPOSE 8080

ENV _JAVA_OPTIONS "-XX:MinRAMPercentage=60.0 -XX:MaxRAMPercentage=90.0 \
-Djava.security.egd=file:/dev/./urandom \
-Djava.awt.headless=true -Dfile.encoding=UTF-8 \
-Dspring.output.ansi.enabled=ALWAYS \
-Dspring.profiles.active=default"

# set entrypoint to layered Spring Boot application
ENTRYPOINT ["java", "-jar", "spring-boot-example-app-${APP_VERSION}-SNAPSHOT.jar"]

# Metadata
LABEL org.opencontainers.image.vendor="Matt Shields" \
	org.opencontainers.image.url="https://gitlab.com/mattshields/springboot-example-app" \
	org.opencontainers.image.title="Springboot Example App" \
	org.opencontainers.image.description="A sample springboot docker app" \
	org.opencontainers.image.version="${APP_VERSION}" \
	org.opencontainers.image.documentation="https://gitlab.com/mattshields/springboot-example-app"
